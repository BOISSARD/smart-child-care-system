from gpiozero import Button
from gpiozero import LED
from gpiozero import Buzzer
from time import sleep
import sys


button = Button(17)
led = LED(18)
buzzer = Buzzer(12)

try :
#for i in range(10) :
	while True :
		button.wait_for_press()
		print("pressed")
		led.on()
		buzzer.on()
		button.wait_for_release()
		print("released")
		led.off()
		buzzer.off() 
	
except :
	sys.exit()
