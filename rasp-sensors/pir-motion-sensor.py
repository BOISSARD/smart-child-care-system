import RPi.GPIO as GPIO
from gpiozero import LED
from gpiozero import Buzzer
import time

GPIO.setmode(GPIO.BCM)

GPIO.setup(22, GPIO.IN, GPIO.PUD_UP)
led = LED(18)
buzzer = Buzzer(12)

try :
	time.sleep(2)
	previous_state = False
	current_state = False

	while True :
		previous_state = current_state 
		current_state = GPIO.input(22)
		if current_state : #!= previous_state :
			new_state = "HIGH" if current_state else "LOW" 
			print("GPIO pin %s is %s" % (22, new_state))
			led.on()
			#buzzer.on()
			time.sleep(1)
			led.off()
			#buzzer.off()
		time.sleep(0.1)
		
except :
	GPIO.cleanup()
