# Imports Frameworks and Tools
from flask import Flask
from flask_restful import Api, Resource
from flask_cors import CORS
import json

# Imports Ressources
from sensor import Sensor
from notification import Notification
from raspberrypi import RaspberryPi

# Configuration
app = Flask(__name__)
api = Api(app)
CORS(app, resources={r"/*": {"origins": "*"}})

# Use of the ressources
api.add_resource(Sensor, "/sensor/<string:name>")
@app.route('/sensors')
def getSensors():
    return json.dumps(Sensor.sensors), 200
api.add_resource(RaspberryPi, "/rpi/<string:name>")
api.add_resource(Notification, "/notif/<string:name>")

@app.route('/')
def testConnection():
    return json.dumps(True)

# Listening for the API if not already doing
if __name__ == '__main__':
    import os
    HOST = os.environ.get('SERVER_HOST', 'localhost')
    try:
        PORT = int(os.environ.get('SERVER_PORT', '5555'))
    except ValueError:
        PORT = 5555
    #app.run(HOST, PORT)
    app.run(host='0.0.0.0',port=5555)