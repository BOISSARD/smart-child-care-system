from flask_restful import Resource, reqparse

class RaspberryPi(Resource):
    """description of class"""

    def get(self, name):
        str =  "RPi GET name {}".format(name)
        print(str)
        return str

    def post(self, name):
        str =  "RPi POST name {}".format(name)
        print(str)
        return str
