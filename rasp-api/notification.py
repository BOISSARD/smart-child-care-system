from flask_restful import Resource, reqparse


class Notification(Resource):
    """description of class"""
    
    actions = [{
            "name": "subscribe"
        },{
            "name": "unsubscribe"
        }
    ]

    def get(self, name):
        for action in self.actions:
            if(name == action["name"]) :
                return action, 200
        return "Sensor {} not found".format(name), 404



