from flask_restful import Resource, reqparse

class Sensor(Resource):
    """description of class"""
    
    sensors = [{
            "name": "temperature"
        },{
            "name": "humidity"
        },{
            "name": "motion-driver"
        },{
            "name": "motion-passenger"
        },{
            "name": "sound",
            "script": "sound.py"
        }
    ]

    def get(self, name="all") :
        print(name)
        for sensor in self.sensors:
            if(name == sensor["name"]) :
                return { "name" : sensor["name"], "value" : 0}, 200
        return "Sensor {} not found".format(name), 404