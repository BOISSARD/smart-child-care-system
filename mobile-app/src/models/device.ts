import { CommunicationModes } from '../providers/communications/communication-modes';
import { CommunicationProvider } from '../providers/communications/communication';
import { CommunicationSMS } from '../providers/communications/communication-sms';

export class Device {
  name: string;
  wantNotifications: boolean;

  communication: CommunicationProvider;

  get mode() {
    return CommunicationModes[this.communication.mode];
  }

  constructor(communication: CommunicationProvider, name: string = null, wantNotifs: boolean = true) {
    this.communication = communication;
    this.name = name === null ? this.communication.address : name;
    this.wantNotifications = wantNotifs;
    this.changeNotification();
  }

  equal(device: Device): boolean {
    return this.name == device.name
      && this.communication.address === device.communication.address
      && this.communication.mode === device.communication.mode;
  }

  changeNotification() {
    if (!this.communication.connected) {
      this.communication.connect(
        () => {
          this.updateNotificationSubscription()
        },
        () => {
          this.wantNotifications = false;
        })
      if (!this.communication.connected)
        this.wantNotifications = false;
    } else {
      this.updateNotificationSubscription()
    }
  }

  private updateNotificationSubscription() {
    if (!this.communication.connected) return;

    let communication;
    if (this.wantNotifications)
      communication = this.communication.communicate("notif/subscribe")
    else communication = this.communication.communicate("notif/unsubscribe")

    communication
      .then(data => {
        console.log(data)
      })
      .catch(error => {
        console.error(error)
        this.wantNotifications = false;
      })
  }

}
