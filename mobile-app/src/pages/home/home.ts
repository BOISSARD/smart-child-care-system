import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { Device } from '../../models/device';
import { ConnectionPage } from '../connection/connection';
import { DeviceslistPage } from '../deviceslist/deviceslist';
import { GlobalVarsProvider } from '../../providers/global-vars/global-vars';
import { CommunicationModes } from '../../providers/communications/communication-modes';
import { Connection } from '@angular/http';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  device: Device;
  mode: string;
  toggleConnection: boolean;
  test: string;
  sensors = [];
  nbRefreshed = 0;
  errorAlert;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public alertCtrl: AlertController,
    public global: GlobalVarsProvider) {
    this.device = this.navParams.get('device');

    this.toggleConnection = true;
    this.connect(() => {
      this.getData();
    }, function () {
      alertCtrl.create({
        title: 'Error',
        message: 'Connection to the device impossible.'
      }).present();
    });
  }

  connect(sucess = undefined, error = undefined) {
    if (this.toggleConnection) {
      let connection = this.device.communication.connect()
      if (connection)
        connection
          .then(response => {
            this.toggleConnection = response.json();
            //console.log('sucess', !!sucess);
            if (sucess) sucess();
          })
          .catch(response => {
            this.toggleConnection = false;
            //console.log('error', !!error);
            if (error) error();
          });
    }
    else this.toggleConnection = this.device.communication.disconnect();
  }

  getData(refresher = undefined) {
    //console.log("getdata")
    this.device.communication
      .communicate("sensors")
      .then(sensorsData => {
        //console.log(sensorsData)
        let sensorNames = sensorsData;
        this.sensors = [];
        sensorNames.forEach((s) => {
          this.device.communication
            .communicate("sensor/" + s.name)
            .then(data => {
              this.sensors.push(data);
              if (refresher) this.stopRefresh(refresher, sensorNames.length)
            })
            .catch(error => {
              if (refresher) refresher.cancel()
              if (this.errorAlert) {
                this.errorAlert.setMessage(error.json ? error.json() : error)
              } else {
                this.errorAlert = this.alertCtrl.create({
                  title: "Error",
                  message: error.json ? error.json() : error,
                })
                this.errorAlert.present();
              }
            });
        });
      });
  }

  stopRefresh(refresher, nbToRefresh) {
    this.nbRefreshed += 1;
    if (this.nbRefreshed >= nbToRefresh) {
      refresher.complete();
      this.nbRefreshed = 0;
    }
  }

  modify() {
    this.navCtrl.push(ConnectionPage, { device: this.device });
  }

  delete() {
    let alert = this.alertCtrl.create({
      title: 'Suppression',
      message: 'Do you really want to delete this device ?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Delete',
          handler: () => {
            this.global.removeDevice(this.device);//removeDeviceFromIndex(i);
            this.navCtrl.push(DeviceslistPage);
          }
        }
      ]
    });
    alert.present();
  }

}
