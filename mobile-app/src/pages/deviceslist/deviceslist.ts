import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { GlobalVarsProvider } from '../../providers/global-vars/global-vars';
import { Device } from '../../models/device';
import { CommunicationModes } from '../../providers/communications/communication-modes';
import { HomePage } from '../home/home';
import { ConnectionPage } from '../connection/connection';

/**
 * Generated class for the DeviceslistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-deviceslist',
  templateUrl: 'deviceslist.html',
})
export class DeviceslistPage {

  isDraging: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private alertCtrl: AlertController,
    private global: GlobalVarsProvider) {

    //global.loadDevices()
    //console.log(global.devices)
  }

  ionViewDidLoad() {}

  newDevice() {
    this.navCtrl.push(ConnectionPage);
  }

  modify(device: Device) {
    this.navCtrl.push(ConnectionPage, { device: device });
  }

  delete(device: Device) {
    if (!this.isDraging) {
      let alert = this.alertCtrl.create({
        title: 'Suppression',
        message: 'Do you really want to delete this device ?',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              this.isDraging = false;
            }
          },
          {
            text: 'Delete',
            handler: () => {
              this.global.removeDevice(device);
              this.isDraging = false;
            }
          }
        ]
      });
      alert.present();
      this.isDraging = true;
    }
  }

  displayDevice(device) {
    this.navCtrl.push(HomePage, { device: device });
  }

}
