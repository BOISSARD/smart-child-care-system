import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';

import { HomePage } from '../home/home';

import { CommunicationModes, stringToCommunicationModes } from '../../providers/communications/communication-modes';
import { CommunicationProvider } from '../../providers/communications/communication';
import { CommunicationFactory } from '../../providers/communications/communication-factory';

import { GlobalVarsProvider } from '../../providers/global-vars/global-vars';
import { DeviceslistPage } from '../deviceslist/deviceslist';
import { Device } from '../../models/device';

/**
 * Generated class for the ConnectionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-connection',
  templateUrl: 'connection.html',
})
export class ConnectionPage {

  communicationModesKeysArray: Object[];

  communicationModesSelect;
  communicationValue;
  name;
  device: Device;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private loading: LoadingController, private alertCtrl: AlertController,
    private global: GlobalVarsProvider) {
    this.communicationModesKeysArray = Object.keys(CommunicationModes).filter(k => typeof CommunicationModes[k] === 'number');
    //console.log(this.communicationModesKeysArray)
    this.device = this.navParams.get("device");
    if (this.device) {
      //console.log(this.device);
      this.name = this.device.name;
      this.communicationValue = this.device.communication.address;
      this.communicationModesSelect = CommunicationModes[this.device.communication.mode];
    }
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad ConnectionPage');
  }

  private connection() {
    let newDevice = new Device(CommunicationFactory.createCommunication(this.communicationValue, stringToCommunicationModes(this.communicationModesSelect)), this.name, true);
    if (this.device) {
      this.global.updateDevice(newDevice, this.device)
    } else {
      this.global.addDevice(newDevice);
    }
    this.navCtrl.push(DeviceslistPage);
  }

}
