import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { Http, HttpModule } from '@angular/http';

import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { IonicStorageModule } from '@ionic/storage';

import { GlobalVarsProvider } from '../providers/global-vars/global-vars';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ConnectionPage } from '../pages/connection/connection';
import { DeviceslistPage } from '../pages/deviceslist/deviceslist';
import { HttpProvider } from '../providers/http/http';
import { RaspberryPiApiProvider } from '../providers/raspberry-pi-api/raspberry-pi-api';
import { StorageProvider } from '../providers/storage/storage';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ConnectionPage,
    DeviceslistPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ConnectionPage,
    DeviceslistPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    GlobalVarsProvider,
    HttpProvider,
    RaspberryPiApiProvider,
    StorageProvider
  ]
})
export class AppModule {}
