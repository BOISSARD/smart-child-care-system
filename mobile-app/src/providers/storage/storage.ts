import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

import { Device } from '../../models/device';

/*
  Generated class for the StorageProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

const DEVICES_KEY = 'my-devices';

@Injectable()
export class StorageProvider {

  constructor(private storage: Storage) { }

  addDevice(device: Device): Promise<any> {
    return this.storage
      .get(DEVICES_KEY)
      .then((devices: Device[]) => {
        if (devices) {
          devices.push(device);
          return this.storage.set(DEVICES_KEY, devices);
        } else {
          return this.storage.set(DEVICES_KEY, [device]);
        }
      })
  }

  clearDevices(): Promise<any> {
    return this.storage.set(DEVICES_KEY, []);
  }

  getDevices(): Promise<Device[]> {
    let dk = this.storage.get(DEVICES_KEY);
    return dk;
  }

  updateDevice(newDevice: Device, oldDevice: Device): Promise<any> {
    //console.log(newDevice, oldDevice)
    return this.storage
      .get(DEVICES_KEY)
      .then((devices: Device[]) => {
        if (!devices || devices.length === 0) return null;
        let newDevices: Device[] = [];
        //console.log(devices)
        for (let d of devices) {
          if (oldDevice.equal(d)) {
            newDevices.push(newDevice);
          } else {
            newDevices.push(d);
          }
        }
        return this.storage.set(DEVICES_KEY, newDevices);
      })
  }

  deleteDevice(device: Device): Promise<Device> {
    //console.log(device)
    return this.storage
      .get(DEVICES_KEY)
      .then((devices: Device[]) => {
        if (!devices || devices.length === 0) return null;
        let toKeep: Device[] = [];
        for (let d of devices) {
          if (!device.equal(d)) {
            toKeep.push(d);
          }
        }
        return this.storage.set(DEVICES_KEY, toKeep);
      })
  }

}
