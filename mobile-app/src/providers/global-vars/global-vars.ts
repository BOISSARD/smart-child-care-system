import { Injectable } from '@angular/core';

import { Device } from '../../models/device';
import { CommunicationModes } from '../communications/communication-modes';
import { CommunicationFactory } from '../communications/communication-factory';
import { HttpProvider } from '../http/http';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { StorageProvider } from '../storage/storage';
import { Platform, ToastController } from 'ionic-angular';
//import { NativeStorage } from '@ionic-native/native-storage/ngx';

/*
  Generated class for the GlobalVarsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GlobalVarsProvider {

  private _devices: Device[];

  constructor(private http: HttpProvider, private storage: StorageProvider, private toastCtrl: ToastController) {
    CommunicationFactory.http = http;
    
    if (!this._devices) {
      this._devices = [];
      //this.storage.clearDevices();
      this.loadDevices();
    }
  }

  public get devices(): Device[] {
    return this._devices;
  }

  loadDevices() {
    this.storage
      .getDevices()
      .then(devices => {
        const toast = this.toastCtrl.create({
          message: devices.length + " device(s) saved",
          duration: 3000
        });
        toast.present();
        let newDevices = [];
        for (let d of devices) {
          newDevices.push(new Device(CommunicationFactory.createCommunication(d.communication.address, d.communication.mode), d.name, d.wantNotifications));
        }
        this._devices = newDevices;
      })
      .catch(err => this.toatError(err));
  }

  public addDevice(device: Device) {
    this.storage
      .addDevice(device)
      .then(d => {
        this.loadDevices()
      })
      .catch(err => this.toatError(err));
  }

  public updateDevice(newDevice: Device, oldDevice: Device) {
    this.storage
      .updateDevice(newDevice, oldDevice)
      .then(d => {
        this.loadDevices();
      })
      .catch(err => this.toatError(err));
  }

  public removeDevice(device: Device) {
    this.storage
      .deleteDevice(device)
      .then(d => {
        this.loadDevices()
      })
      .catch(err => this.toatError(err));
  }

  public removeDeviceFromIndex(index: number) {
    this._devices.splice(index, 1);
  }

  public getDeviceIndex(device: Device) {
    return this.devices.findIndex(d => device.equal(d));
  }

  public toatError(error) {
    console.error(error);
    const toast = this.toastCtrl.create({
      message: error,
      duration: 5000
    });
    toast.present();
  }

}
