import { Injectable } from '@angular/core';

import { CommunicationProvider } from '../communications/communication';
import { CommunicationModes } from './communication-modes';
/*
  Generated class for the GlobalVarsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CommunicationSMS extends CommunicationProvider {

  constructor(address: string) {
    super(address, CommunicationModes.SMS);
  }


  connect(sucess?, error?) {
    this.connected = true;
  }

  disconnect() {
    this.connected = false;
  }

  communicateWithData(request: string, data: string) {
    //console.log(request, data);
    return new Promise((resolve, reject) => {
      window.setTimeout(() => {
        resolve("result sms");
      }, 1000)
    });
  }  
  
}
