
export enum CommunicationModes {
  SMS,//= "SMS via GSM",
  Internet,// = "Internet (3G/4G/LTE)",
  Bluetooth,// = "Bluetooth"

}

export function stringToCommunicationModes(mode: string): CommunicationModes {
  switch (mode) {
    case "SMS": return CommunicationModes.SMS;
    case "Internet": return CommunicationModes.Internet;
    case "Bluetooth": return CommunicationModes.Bluetooth;
  }
  return null;
}
