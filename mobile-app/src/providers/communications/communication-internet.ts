//import { Injectable } from '@angular/core';

import { CommunicationProvider } from '../communications/communication';
import { CommunicationModes } from './communication-modes';
import { HttpProvider } from '../http/http';
import { Http } from '@angular/http';
/*
  Generated class for the GlobalVarsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

//@Injectable()
export class CommunicationInternet extends CommunicationProvider {

  private url: string;
  private http: HttpProvider;

  constructor(address: string, private httpd: HttpProvider) {
    super(address, CommunicationModes.Internet);
    this.url = `http://${this.address}`;
    this.http = httpd;
  }

  connect(sucess?, error?) {
    return this.testConnection()
  }

  disconnect() {
    this.connected = false;
    return this.connected;
  }

  private testConnection() {
    let resp = this.http.request('get', `${this.url}/`);
    resp
      .then(respond => {
        respond = respond.json();
        this.connected = respond;
        //console.log(respond);
      })
      .catch(error => {
        this.connected = false;
      });
    return resp;
  }

  communicateWithData(request: string, data: string) {
    const url = `${this.url}/${request}`;
    //console.log(url, !data);
    let response;
    if (!data)
      response = this.http.request('get', url);
    else
      response = this.http.request('post', url, data);
    return response.then(respond => respond.json());
  }

}
