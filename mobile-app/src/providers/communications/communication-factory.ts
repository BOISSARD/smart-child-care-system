import { CommunicationProvider } from "./communication";
import { CommunicationModes } from "./communication-modes";
import { CommunicationSMS } from "./communication-sms";
import { CommunicationInternet } from "./communication-internet";
import { HttpProvider } from "../http/http";
import { Http } from "@angular/http";
import { CommunicationBluetooth } from "./communication-bluetooth";

export class CommunicationFactory {

  //static constructor(private http: Http) { }

  private static _http: HttpProvider;
  public static set http(http: HttpProvider) {
    this._http = http;
  }

  static createCommunication(address: string, mode: CommunicationModes) {
    let communicationProvider: CommunicationProvider = null;
    switch (mode) {
      case CommunicationModes.SMS:
        //console.log("CommunicationModes.SMS");
        communicationProvider = new CommunicationSMS(address);
        break;
      case CommunicationModes.Internet:
        //console.log("CommunicationModes.Internet");
        //communicationProvider = null;
        communicationProvider = new CommunicationInternet(address, this._http);
        break;
      case CommunicationModes.Bluetooth:
        //console.log("CommunicationModes.Bluetooth");
        //communicationProvider = null;
        communicationProvider = new CommunicationBluetooth(address);
        break;
    }
    return communicationProvider;
  }

}
