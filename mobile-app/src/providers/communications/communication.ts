import { Injectable } from '@angular/core';
import { CommunicationModes } from './communication-modes';
import { CommunicationSMS } from './communication-sms';

//@Injectable()
export abstract class CommunicationProvider {

  public connected: boolean;

  public mode: CommunicationModes;

  public address: string;

  constructor(address: string, mode: CommunicationModes) {
    this.connected = false;
    this.address = address;
    this.mode = mode;
  }

  abstract connect(sucess?, error?);

  abstract disconnect();

  //communicate(request: string): any;/* {
  communicate(request: string) {
    return this.communicateWithData(request, null);
  }/**/

  abstract communicateWithData(request: string, data: string): any;
  //abstract communicate(request: string, data: string): any;
  
}
