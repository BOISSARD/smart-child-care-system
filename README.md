# Smart Child Care System

## Description
The code of the IoT device of a Raspberry Pi, that is a smart child care system against heat stroke in vehicle.

## Functionalities :
 - Detect if there is a living being, on the driver seat and elsewhere.
 - If there is someone and the driver/care-giver quit the vehicle, trigger an alarm.
 - If there is someone and the driver/care-giver is not there, inform him on an app.
 - Provide a way for the user to have acces to some informations like temperature in the vehicle, or the GPS position .

## Equipment :
 - A Raspberry Pi 3, with bluetooth, on wich is running a raspbian.
 - Motion sensors, one for the driver and one for passengers.
 - Sound sensor, to detect living being crying or something else.
 - Temperature and Humidity sensor, to have the environment state inside.
